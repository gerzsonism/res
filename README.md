# 🏠 RES - Real Estate Search *(Service)*

### 🛠️ Steps to configure
---

After running `npm i`, create a `config.json` file at the root of your directory, with the following parameters:
```json
{
    // Price of the estate in millions
    "price": {
        "min": 5,
        "max": 20
    },
    // Size of flat in m²
    "size": {
        "min": 40,
        "max": 100
    },
    // The number of rooms to include in the results
    "numberOfRooms": {
        "min": 1,
        "max": 3
    },
    // List of services to be omitted from the results
    // To find out the available name of services, open `app/services/index.js`
    // ⚠️ Services listed here as an example are known to have slow load times.
    // It is recommended to blacklist them to speed up searching.
    "blacklistServices": [
        "artHome",
        "flafty",
        "koltozzBe"
        "startapro", // Mostly contains unrelated locations
        "Ingatlan7" // Mostly contains mostly out-dated entries
    ],
    // List of keywords to be matched and removed from the generated result set
    // Used for `npm run filter`
    "blacklistResults": [
        "tarjan",
        "panel"
    ]
}
```

You can also find an example config file, called `config.example.json`.

### 💻 CLI commands
---
| command | description |
|:--|:--|
| `npm run start`   | Start search |
| `npm run sort`    | Sort the result set, after the search has been completed |
| `npm run filter`  | Filter the result set, after the search has been completed |
| `npm run diff`    | Diff two result set, to collect new uploads |

> ℹ️ If you would like to collect new uploads, rename your `results.md` to `results1.md`, then re-run `npm run start`. After a new `results.md` has been generated, run `npm run diff` that will generate a `diff.md` file with only the new uploads.

> ℹ️ If you would like to tweak sorting and filtering, open `scripts/sort.js` or `scripts/filter.js`

> ℹ️ Note that ingatlanTájoló may have broken links, where the base url is missing (https://www.ingatlantajolo.hu)