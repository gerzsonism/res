// Libraries
const libraries = {
    search: require('./lib/search.js')
}

module.exports = {
    search: libraries.search
};
