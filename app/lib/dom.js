module.exports = {
    getElementProperty(element, property) {
        const attributes = element.attributes;
        let propertyValue;

        attributes.forEach(attribute => {
            if (attribute.name === property) {
                propertyValue = attribute.value;
            }
        });

        return propertyValue;
    }
};