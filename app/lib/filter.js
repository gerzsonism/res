const parse = require('./parser.js');
const logger = require('./logger.js');

const blacklist = [
    '(bérleti)',
    '(garázs)'
];

// Services that will always contain a keyword from blacklist
const whitelist = [
    'jofogas',
    'ingatlanCafe',
    'palankIngatlan'
];

const regex = new RegExp(blacklist.join('|'), 'gi');

module.exports = async results => {
    const filteredResults = [];

    for (const service of Object.keys(results)) {
        const title = results[service].title;
        const filteredProperties = [];
        const total = results[service].results.length
        let current = 0;

        logger.progress(`Filtering ${service}`);

        for (const result of results[service].results) {
            if (result.url && result.url.startsWith('http')) {
                const document = await parse(encodeURI(result.url));
                const progress = ((current / total) * 100).toFixed(2);
    
                if (!regex.test(document.rawHTML) || whitelist.includes(service)) {
                    filteredProperties.push(result);
                }
    
                logger.logProgress(`Filtered ${current} properties, from a total of ${total} (${progress}%)`);
    
                current++;
            } else {
                filteredProperties.push(result);
            }
        }

        logger.success(`Filtered ${service}`);

        filteredResults.push({
            title,
            results: filteredProperties
        });
    }

    return filteredResults;
};