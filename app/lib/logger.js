module.exports = {
    progress(message) {
        console.log(`🏃 ${message}...`);
    },

    logProgress(message) {
        process.stdout.clearLine();
        process.stdout.cursorTo(0);
        process.stdout.write(`🏃 ${message}...`);
    },

    info(message) {
        console.log(`📘 ${message}`)
    },

    success(message) {
        console.log(`✅ ${message}`);
    }
};