module.exports = {
    createHeading(name, url) {
        return `## [${name}](${url})`;
    },

    createListItem(name, url) {
        return `- [${name}](${url})`;
    },

    createDivider() {
        return '---';
    }
};