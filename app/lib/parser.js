const fetch     = require('node-fetch');
const DOMParser = require('dom-parser');

module.exports = url => {
    return fetch(url).then(res => res.text())
        .then(HTMLResponse => {
            return new DOMParser().parseFromString(HTMLResponse, 'text/html');
        });
};