const dom = require('./dom.js');
const logger = require('./logger.js');
const parse = require('./parser.js');

let page = 1;

const propertyCollector = async props => {
    const {
        config,
        service,
        document,
        selectors,
        useBaseUrl = true,
        useLocation = false,
        useLinkText = false,
        usePriceOnly = false,
        useCustomText = false,
        baseUrl,
        getNextPageUrl,
        prependLinkUrl,
        priceInContainer = false,
        linkInContainer = false,
        linkIndex,
        priceTagName,
        hasPropertyDescription = false,
        getPropertyDescription,
        customFilter,
        cardIsLink = false,
        collectedProperties
    } = props;

    const links = collectedProperties || [];

    // Filters out properties with text like "Bérleti jog"
    const filterRegex = /jog/gi
    const queryRegex = /(&#038;)|(&amp;)/g;

    const nextPageUrl = getNextPageUrl(document);

    // Filter for relevant properties
    let properties = [...document.getElementsByClassName(selectors.propertyCard)].filter(property => {
        let priceNode = property.getElementsByClassName(selectors.propertyPrice)[0];
        let propertyDescription = '';

        // Collect price
        if (priceInContainer) {
            const priceContainerNode = property.getElementsByClassName(selectors.propertyPrice)[0] || property;
            
            // If there's no price, force filtering
            if (priceContainerNode) {
                priceNode = priceContainerNode.getElementsByTagName(priceTagName)[0];
            } else {
                priceNode = { textContent: '999' };
            }
        }
        
        // If there's no price, force filter by high value
        const priceText = priceNode ? priceNode.textContent.trim() : '999';
        const propertyPrice = Number(priceText.split(/[\.,\s]/)[0].replace(/\D/g, ''));

        // Collect description
        if (hasPropertyDescription) {
            propertyDescription = getPropertyDescription(property);
        }

        if (config.price.max > propertyPrice && !filterRegex.test(propertyDescription)) {
            return true;
        }
    
        return false;
    });

    // Filter down properties if a custom filtering is defined
    if (customFilter) {
        properties = properties.filter(property => customFilter(property));
    }

    // Collect urls
    properties.forEach(property => {
        let priceNode = property.getElementsByClassName(selectors.propertyPrice)[0];
        let propertyDescription;
        let anchorNode;
        let listItemText;
        let link;

        // Collect link from anchor
        if (!linkInContainer) {
            // If the link is the property card itself
            if (cardIsLink) {
                anchorNode = property;
                link = dom.getElementProperty(property, 'href');
            } else {
                anchorNode = property.getElementsByClassName(selectors.propertyLink)[0];
                link = anchorNode && dom.getElementProperty(anchorNode, 'href');
            }
        } else {
            const titleNode = property.getElementsByClassName(selectors.propertyLink)[0] || property;

            anchorNode = titleNode.getElementsByTagName('a')[linkIndex || 0];
            link = dom.getElementProperty(anchorNode, 'href');
        }

        // Collect price
        if (priceInContainer) {
            const priceContainerNode = property.getElementsByClassName(selectors.propertyPrice)[0] || property;
            
            priceNode = priceContainerNode.getElementsByTagName(priceTagName)[0];
        }

        // Collect description
        if (hasPropertyDescription) {
            propertyDescription = getPropertyDescription(property);
        }
        
        const priceText = priceNode.textContent.trim();
        const propertyPrice = Number(priceText.split(/[\.,\s]/)[0].replace(/\D/g, ''));

        listItemText = `${propertyPrice}M, ${propertyDescription}`;

        // Use link text
        if (useLinkText && anchorNode) {
            listItemText = `${propertyPrice}M, ${anchorNode.textContent.trim()}`;
        }

        // Use location
        if (useLocation) {
            const locationNode = property.getElementsByClassName(selectors.propertyLocation)[0];
            const locationText = locationNode.textContent.trim();
            
            listItemText = `${propertyPrice}M, ${locationText}`;
        }

        // Use size (m²)

        // Use a custom text, returned from a callback function
        if (useCustomText) {
            listItemText = `${propertyPrice}M, ${useCustomText(property)}`;
        }

        // Use only price for text
        if (usePriceOnly) {
            listItemText = `${propertyPrice}M`;
        }
        
        if (anchorNode) {
            links.push({
                title: listItemText,
                url: `${prependLinkUrl}${link.replace(queryRegex, '&')}`
            });
        }
    });
    
    logger.success(` - Collected estates from ${service} on page ${page}`);
    
    
    if (nextPageUrl) {
        const absoluteNextPageUrl = useBaseUrl ? `${baseUrl}${nextPageUrl.replace(queryRegex, '&')}` : nextPageUrl.replace(queryRegex, '&');
        const document = await parse(absoluteNextPageUrl);

        page++;

        // Limit results to 100 pages to prevent accidental infinite loops (only experienced on Flafty)
        if (page >= 100) {
            return links;
        }
    
        await propertyCollector({
            ...props,
            document,
            collectedProperties: links
        });
    }

    page = 1;

    return links;
};

module.exports = propertyCollector;