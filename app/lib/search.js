const fs = require('fs');
const filter = require('./filter.js');
const services = require('../services/index.js');
const libraries = {
    collectProperties : require('../lib/propertyCollector.js'),
    urlBuilder        : require('../lib/urlBuilder.js'),
    parse             : require('../lib/parser.js'), 
    markdown          : require('../lib/markdown.js'),
    logger            : require('../lib/logger.js'),
    dom               : require('../lib/dom.js')
};

module.exports = async config => {
    const results = {};
    const file = [];

    if (config.blacklistServices) {
        config.blacklistServices.forEach(service => {
            delete services[service];
        });
    }

    for (const key of Object.keys(services)) {
        const serviceResult = await services[key]({
            ...config,
            ...libraries
        });

        const title = serviceResult.shift();
    
        results[key] = {
            title,
            results: serviceResult
        };
    }

    const totalResults = Object.keys(results).map(key => results[key].results.length).reduce((acc, current) => acc + current, 0);

    libraries.logger.info(`Collected ${totalResults} results from ${Object.keys(services).length} services`);
    libraries.logger.progress('Filtering results');

    const filteredResults = await filter(results);
    const totalFilteredResults = Object.keys(filteredResults).map(key => filteredResults[key].results.length).reduce((acc, current) => acc + current, 0);

    libraries.logger.info(`Removed ${totalResults - totalFilteredResults} properties from a total of ${totalResults} (${totalFilteredResults} remaining)`);

    for (const key of Object.keys(filteredResults)) {
        file.push(filteredResults[key].title);

        filteredResults[key].results.forEach(result => {
            file.push(libraries.markdown.createListItem(result.title, result.url));
        });
    }

    // Add divider between services
    const uniqueFile = [...new Set(file)];
    const endResult = uniqueFile.map((line, index) => {
        if (line.startsWith('##') && index !== 0) {
            return [
                libraries.markdown.createDivider(),
                '',
                line
            ];
        }

        return line;
    }).flat(1);

    // TODO - fix number of total results, it includes the headers and separators (## ---)
    const numberOfTotalResults = totalFilteredResults - (file.length - uniqueFile.length);

    libraries.logger.info(`Removed ${file.length - uniqueFile.length} duplicate entries. (${numberOfTotalResults} remaining)`);

    fs.writeFileSync('results.md', endResult.join('\n'));

    libraries.logger.success(`Collected ${numberOfTotalResults} results to 📁 results.md`);
};