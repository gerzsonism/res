module.exports = async config => {
    const {
        size,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom,
        collectProperties
    } = config;

    const selectors = {
        propertyCard: 'gallery__card-container',
        propertyPrice: 'estate-card__estate-price',
        propertyLink: 'gallery__card--calc-size',
        propertyLocation: 'estate-card__estate-address--street-or-zone',
        propertyDetails: 'estate-card__details',
        propertySize: 'estate-card__detail-value',
        paginator: 'pager__next'
    };

    const service = 'Agenda Ház';

    logger.progress(`Collecting estates from ${service}`);
    
    const url = urlBuilder('agendaHaz', `+${size.min}-${size.max}-m2`);
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLocation: true,
        useBaseUrl: true,
        baseUrl: 'https://agendahaz.ingatlan.com',
        prependLinkUrl: 'https:',
        getNextPageUrl(document) {
            const nextPageNode = document.getElementsByClassName(selectors.paginator)[0];
            const nextPageUrl = nextPageNode && dom.getElementProperty(nextPageNode, 'href');

            return nextPageUrl;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};