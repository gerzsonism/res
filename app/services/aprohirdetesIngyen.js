module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'h0_elem',
        propertyPrice: 'h_ar',
        propertyDetails: 'h_intro',
        propertyLink: 'h_nev',
        paginator: 'h_oldal_kovetkezo'
    };

    const service = 'Apróhírdetés Ingyen';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('aprohirdetesIngyen', '');
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        useLinkText: true,
        linkInContainer: true,
        hasPropertyDescription: true,
        baseUrl: 'https://aprohirdetesingyen.hu',
        prependLinkUrl: 'https://aprohirdetesingyen.hu',
        hasPropertyDescription: true,
        getPropertyDescription(property) {
            return property.getElementsByClassName(selectors.propertyDetails)[0].textContent;
        },
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];
            const nextPageUrl = paginationNode && dom.getElementProperty(paginationNode, 'href');

            return nextPageUrl;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};