module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to Arian Property');

    return [
        markdown.createHeading('Arian Property', urlBuilder('arian', ''))
    ];
};