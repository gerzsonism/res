module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'listing',
        propertyPrice: 'listing-price',
        propertyDetails: 'propinfo',
        propertyLink: 'marB0',
        paginator: 'next-page-link'
    };

    const service = 'Art Home';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('artHome', '/?ct_mls&ct_ct_status=elado&ct_property_type=0&ct_keyword=Szeged&search-listings=true&ct_city=0&ct_beds=0&ct_price_from&ct_price_to&ct_sqft_from&ct_sqft_to');
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: false,
        useLinkText: true,
        linkInContainer: true,
        prependLinkUrl: 'https:',
        hasPropertyDescription: true,
        getPropertyDescription(property) {
            const propertyDescription = property.getElementsByClassName(selectors.propertyDetails)[0];
            const propertyDescriptionParagraph = propertyDescription.getElementsByTagName('p')[0].textContent;

            return propertyDescriptionParagraph;
        },
        getNextPageUrl(document) {
            const paginationNode = document.getElementById(selectors.paginator);
            const paginationAnchor = paginationNode && paginationNode.getElementsByTagName('a')[0];
            const nextPageUrl = paginationAnchor && dom.getElementProperty(paginationAnchor, 'href');

            return nextPageUrl;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};