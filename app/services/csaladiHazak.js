module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to Családi Házak');

    return [
        markdown.createHeading('Családi Házak', urlBuilder('csaladiHazak', ''))
    ];
};