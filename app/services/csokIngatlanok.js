module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to CSOK Ingatlanok');

    return [
        markdown.createHeading('CSOK Ingatlanok', urlBuilder('csokIngatlanok', ''))
    ];
};