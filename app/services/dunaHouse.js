module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        numberOfRooms,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'propertyListItem',
        propertyPrice: 'priceBox',
        propertyDetails: 'listItemContent',
        propertyLink: 'moreDetailsBox',
        paginator: 'pagination'
    };

    const service = 'Duna House';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('dunaHouse', `/-/${price.min}-${price.max}-mFt/${size.min}-${size.max}-m2/${numberOfRooms.min}-${numberOfRooms.max}-szoba`);
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        baseUrl: 'https://dh.hu',
        useBaseUrl: true,
        linkInContainer: true,
        prependLinkUrl: 'https://dh.hu',
        hasPropertyDescription: true,
        getPropertyDescription(property) {
            const propertyDescription = property.getElementsByClassName(selectors.propertyDetails)[0];
            const propertyDescriptionParagraph = propertyDescription.getElementsByTagName('h2')[0].textContent;
        
            return propertyDescriptionParagraph;
        },
        getNextPageUrl(document) {
            const paginationContainer = document.getElementsByClassName(selectors.paginator)[0];
            const paginationItems = [...paginationContainer.childNodes].filter(node => node.nodeName === 'li');
        
            const nextPageNode = paginationItems[paginationItems.length - 2];
            const isLastPage = dom.getElementProperty(nextPageNode, 'class') === 'active';
            const nextPageUrl = isLastPage ? null : dom.getElementProperty(nextPageNode.getElementsByTagName('a')[0], 'href');
    
            return nextPageUrl;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};