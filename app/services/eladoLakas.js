module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'result-row',
        propertyPrice: 'price',
        propertyDetails: 'description',
        propertyLink: 'title-link'
    };

    const service = 'Eladó Lakás';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('eladoLakas', `:${price.max}Mft-ig;${size.max}nm-ig;tegla-epitesu?record=0&num=100`);
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLinkText: true,
        priceInContainer: true,
        priceTagName: 'div',
        prependLinkUrl: '',
        hasPropertyDescription: true,
        getPropertyDescription(property) {
            return property.getElementsByClassName(selectors.propertyDetails)[0].textContent;
        },
        getNextPageUrl(document) {
            // 100 properties are shown in first page, at the writing of this using a paginator is unnecessary
            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};