module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'u-shadow-v39',
        propertyPrice: 'g-mt-10',
        propertyDetails: 'g-color-gray-dark-v5',
        propertyLink: 'u-link-v5'
    };

    const service = 'VING';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('eladoLakasSzeged', '');
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLinkText: true,
        prependLinkUrl: '',
        hasPropertyDescription: true,
        getPropertyDescription(property) {
            return property.getElementsByClassName(selectors.propertyDetails)[0].textContent;
        },
        getNextPageUrl(document) {
            // All properties are shown in one page, paginator is unnecessary
            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};