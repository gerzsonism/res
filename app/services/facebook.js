module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to Facebook');

    return [
        markdown.createHeading('Facebook', urlBuilder('facebook', ''))
    ];
};