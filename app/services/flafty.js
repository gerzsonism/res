module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'realty-preview',
        propertyPrice: 'realty-preview__price',
        propertyDetails: 'realty-preview__description',
        propertyLink: 'realty-preview__title-link',
        paginator: 'paging-nav--right'
    };

    const service = 'Flafty';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('flafty', `&area_min=${size.min}&area_max=${size.max}&price_min=${price.min}000000&price_max=${price.max}000000`);
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        baseUrl: 'https://hu.flatfy.com',
        useBaseUrl: true,
        useLinkText: true,
        prependLinkUrl: 'https://hu.flatfy.com',
        hasPropertyDescription: true,
        getPropertyDescription(property) {
            const propertyDescriptionNode = property.getElementsByClassName(selectors.propertyDetails)[0];
            const propertyDescription = propertyDescriptionNode ? propertyDescriptionNode.textContent : '';
        
            return propertyDescription;
        },
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];
            const nextPageUrl = paginationNode && dom.getElementProperty(paginationNode, 'href');
            
            return nextPageUrl;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};