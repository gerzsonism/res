module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        numberOfRooms,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'list-item-container',
        propertyPrice: 'price-line',
        propertyLink: 'overlay-container',
        propertyDetails: 'address',
        propertyLocation: 'address',
        paginator: 'pagination'
    };

    const params = [
        `ar_min=${price.min}`,
        `ar_max=${price.max}`,
        `terulet_min=${size.min}`,
        `terulet_max=${size.max}`,
        `szoba_min=${numberOfRooms.min}`,
        `szoba_max=${numberOfRooms.max}`,
        'erkely_min',
        'erkely_max',
        'emelet_min=0',
        'emelet_max=0'
    ].join('&');

    const service = 'GDN Ingatlan';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('gdnIngatlan', `&${params}`);
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        baseUrl: 'https://www.gdn-ingatlan.hu',
        useBaseUrl: true,
        useLocation: true,
        prependLinkUrl: 'https://www.gdn-ingatlan.hu',
        getNextPageUrl(document) {
            const paginationContainer = document.getElementsByClassName(selectors.paginator)[0];
            let nextPageUrl;

            if (paginationContainer) {
                const paginationItems = [...paginationContainer.childNodes].filter(node => node.nodeName === 'li');
                
                const nextPageNode = paginationItems[paginationItems.length - 1];
                const nextPageAnchor = nextPageNode.getElementsByTagName('a')[0];
                
                nextPageUrl = nextPageAnchor && dom.getElementProperty(nextPageAnchor, 'href');
            }

            return nextPageUrl;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};