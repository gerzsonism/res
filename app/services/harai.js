module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to Hárai');

    return [
        markdown.createHeading('Hárai', urlBuilder('harai', ''))
    ];
};