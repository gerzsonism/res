module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        markdown,
        logger,
        parse,
    } = config;

    const selectors = {
        propertyCard: 'right_box_inner',
        propertyPrice: 'right_box_line_1_right',
        propertyDetails: 'right_box_line_3',
        propertyLink: 'right_box_line_1',
        paginator: 'yiiPager'
    };

    const params = [
        `ar1=${price.min}`,
        `ar2=${price.max}`,
        `alapterulet1=${size.min}`,
        `alapterulet2=${size.max}`,
        'set_pages=999',
        'smallsearch=0',
        'city=Szeged',
        'varos=',
        'ingatlanok_tipus=',
        'ugyvitel_tipusa=elado'
    ].join('&');

    const service = 'Home and People';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('homeAndPeople', `/search?${params}`);
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        baseUrl: 'http://homeandpeople.hu',
        useBaseUrl: true,
        linkInContainer: true,
        useLinkText: true,
        prependLinkUrl: 'http://homeandpeople.hu',
        customFilter(property) {
            const blackList = [
                'Nyaraló',
                'Garázs',
                'Üzlethelyiség',
                'Mezőgazdasági',
                'Telek',
                'Iroda'
            ];

            const titleNode = property.getElementsByClassName(selectors.propertyLink)[0];
            const anchorNode = titleNode.getElementsByTagName('a')[0];

            return !new RegExp(blackList.join('|')).test(anchorNode.textContent);
        },
        getNextPageUrl(document) {
            // Results can be forced into one page, by the set_pages param
            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};