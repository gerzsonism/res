module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        numberOfRooms,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'property_wrapper',
        propertyPrice: 'box_type',
        propertyLink: 'WhiteSquare',
        propertyLocation: 'small_title',
        paginator: 'pagination'
    };

    const params = [
        `price_min:${price.min}000000`,
        `price_max:${price.max}000000`,
        `area_min=${size.min}`,
        `area_max=${size.max}`,
        `rooms_min=${numberOfRooms.min}`,
        `rooms_max=${numberOfRooms.max}`,
        'level_min:1',
        'level_max:14',
        'structure:',
        'condition:',
        'heating:',
        'type%5B1%5D:2',
        'type%5B0%5D:1'
    ].join('/');

    const service = 'Homing Szeged';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('homingSzeged', `/${params}`);
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        baseUrl: 'https://homingszeged.hu',
        useBaseUrl: true,
        useLinkText: true,
        useLocation: true,
        linkInContainer: true,
        prependLinkUrl: 'https://homingszeged.hu',
        getNextPageUrl(document) {
            const paginationContainer = document.getElementsByClassName(selectors.paginator)[0];
            const paginationListItems = [...paginationContainer.childNodes].filter(node => node.nodeName === 'li');
            const nextPageNode = paginationListItems[paginationListItems.length - 1];
            const hasNextPage = dom.getElementProperty(nextPageNode, 'class') === 'next';
            let nextPageUrl;

            if (hasNextPage) {
                nextPageUrl = dom.getElementProperty(nextPageNode.getElementsByTagName('a')[0], 'href');
            }

            return nextPageUrl;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};