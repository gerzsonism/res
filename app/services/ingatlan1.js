module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        markdown,
        logger,
        parse
    } = config;

    const selectors = {
        propertyCard: 'list-item',
        propertyPrice: 'item-price',
        propertyLink: 'item-left-side',
        propertyLocation: 'item-title',
        paginator: 'pages-prevnext'
    };

    const params = [
        `a:${price.min};${price.max}`,
        `t:${size.min};${size.max}`
    ].join('/');

    const service = 'Ingatlan1';
    let pageNumber = 1;

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlan1', `/${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: false,
        useLocation: true,
        linkInContainer: true,
        prependLinkUrl: '',
        customFilter(property) {
            const blackList = [
                'Tarján'
            ];

            const propertyLocation = property.getElementsByClassName(selectors.propertyLocation)[0];

            return !new RegExp(blackList.join('|')).test(propertyLocation.textContent);
        },
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationNode) {
                const paginationButton = paginationNode.getElementsByClassName('right')[0];
                
                if (paginationButton) {
                    pageNumber++;
                    return `${url}/lp${pageNumber}/`;
                }
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};