module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'ehird',
        propertyPrice: 'ar',
        propertyLink: 'leiras',
        propertyLocation: 'item-title',
        paginator: 'k'
    };

    const service = 'Ingatlan7';
    const pages = [];
    let currentPage = -1;

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlan7', '');
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        baseUrl: 'http://www.ingatlan7.com/',
        linkInContainer: true,
        prependLinkUrl: 'http://www.ingatlan7.com/',
        useCustomText(property) {
            const propertyDescriptionNode = property.getElementsByClassName('leiras')[0];
            const propertyTitleNode = propertyDescriptionNode.getElementsByTagName('b')[0];

            return propertyTitleNode.textContent;
        },
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationNode) {
                if (!pages.length) {
                    const links = [...paginationNode.childNodes].filter(node => node.nodeName === 'a');
                    
                    links.forEach(link => {
                        pages.push(dom.getElementProperty(link, 'href'));
                    });
                }

                currentPage++;

                return pages[currentPage];
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};