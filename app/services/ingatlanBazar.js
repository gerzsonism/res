module.exports = config => {
    const {
        price,
        size,
        numberOfRooms,
        urlBuilder,
        markdown,
        logger
    } = config;

    const customParams = `-${price.max}-millio-Ft-ig-${size.min}m2-tol-${size.max}m2-ig`;
    const queryParams  = [
        'property_location=1099',
        `price_min=${price.min}`,
        `price_max=${price.max}`,
        'property_type=off',
        'property_imageonly=on',
        'property_newbuildonly=off',
        'property_owneronly=off',
        `property_floor_area=${size.min}-${size.max}`,
        `property_rooms=${numberOfRooms.min}-${numberOfRooms.max}`,
        'property__2=3_2',
        'page=1',
        'list-order=0',
        '_locale=hu#/search;preventRefresh=true/list'
    ].join('&');

    const url = urlBuilder('ingatlanBazar', [customParams, queryParams].join('?'));

    logger.success('Collected estates from IngatlanBazár');

    return [
        markdown.createHeading('Ingatlanbazár', url),
    ];
};