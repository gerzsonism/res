module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        numberOfRooms,
        markdown,
        logger,
        parse
    } = config;

    const selectors = {
        propertyCard: 'type-rent',
        propertyPrice: 'price',
        propertyLink: 'property-info',
        propertyDescription: 'property-info'
    };

    const params = [
        `ar_min=${price.min}`,
        `ar_max=${price.max}`,
        `terulet_min=${size.min}`,
        `terulet_max=${size.max}`,
        `szoba_min=${numberOfRooms.min}`,
        `szoba_max=${numberOfRooms.max}`,
        'azonosito=',
        'kereses=',
        'ar_forma=1',
        'input83=0',
        'varoskereso=Szeged',
        'tipus=1',
        'kategoria=1',
        'foldal_ker=1'
    ].join('&');

    const service = 'Ingatlan Cafe';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlanCafe', `index.php?${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLinkText: true,
        linkInContainer: true,
        priceInContainer: true,
        priceTagName: 'strong',
        prependLinkUrl: '',
        getNextPageUrl(document) {
            // Pagination is handled by JavaScript, added extra link at the end of links
            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
        {
            title: 'Unable to crawl more pages, see more results from page 2',
            url
        }
    ];
};