module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        numberOfRooms,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'listing',
        propertyPrice: 'price',
        propertyLink: 'listing__link',
        propertyLocation: 'listing__address',
        paginator: 'pagination__button'
    };

    const params = [
        `${price.max}-mFt-ig`,
        `${size.max}-m2-alatt`,
        `${numberOfRooms.min}-${numberOfRooms.max}-szoba`
    ].join('+');

    const service = 'Ingatlan.com';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlanCom', `+${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: false,
        useLocation: true,
        prependLinkUrl: 'https://ingatlan.com/',
        getNextPageUrl(document) {
            const paginationNodes = document.getElementsByClassName(selectors.paginator);
            let paginationNode = paginationNodes[0];

            if (paginationNodes.length === 2) {
                paginationNode = paginationNodes[1];
            }

            const hasNextPage = paginationNode.textContent === 'Következő oldal';

            if (hasNextPage) {
                return dom.getElementProperty(paginationNode, 'href');
            } 

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};