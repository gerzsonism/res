module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to Ingatlan Ház');

    return [
        markdown.createHeading('Ingatlan Ház', urlBuilder('ingatlanHaz', ''))
    ];
};