module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        numberOfRooms,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'listing-item',
        propertyPrice: 'listing-price',
        propertyLink: 'title-full',
        propertyLocation: 'listing-address',
        paginator: 'next'
    };

    const params = [
        `priceMin=${price.min}`,
        `priceMax=${price.max}`,
        `sizeMin=${size.min}`,
        `sizeMax=${size.max}`,
        `roomNumberMin=${numberOfRooms.min}`,
        `roomNumberMax=${numberOfRooms.max}`,
        'order=createdAt-DESC'
    ].join('&');

    const service = 'Ingatlan Maps';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlanMaps', `&${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLinkText: true,
        useBaseUrl: true,
        priceInContainer: true,
        priceTagName: 'span',
        baseUrl: 'https://ingatlanmaps.hu/',
        prependLinkUrl: 'https://ingatlanmaps.hu/',
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationNode) {
                return dom.getElementProperty(paginationNode, 'href');
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};