module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        numberOfRooms,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'item-list-view',
        propertyPrice: 'price-text',
        propertyLink: 'pl-md-0',
        propertyLocation: 'h3',
        paginator: 'next'
    };

    const params = [
        `minPrice=${price.min}`,
        `maxPrice=${price.max}`,
        `plotSizeMin=${numberOfRooms.min}`,
        `plotSizeMax=${numberOfRooms.max}`,
        'status=1',
        'group=2',
        'city=Szeged',
        'aliasIds%5B%5D=1057',
        'roomMin=',
        'areaSizeMin=',
        'areaSizeMax=',
        'buildYearMin=',
        'buildYearMax=',
        'floorMin=',
        'floorMax=',
        'ing_allapot_kod=',
        'ing_falanyag_kod=',
        'ing_futes_kod=',
        'ing_parkolas_kod=',
        'netId=',
        'hasPicture=1',
        'subType%5B%5D=3&subType%5B%5D=2&subType%5B%5D=17'
    ].join('&');

    const service = 'Ingatlannet.hu';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlanNet', `?${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLocation: true,
        useBaseUrl: true,
        linkInContainer: true,
        baseUrl: 'https://www.ingatlannet.hu',
        prependLinkUrl: 'https://www.ingatlannet.hu',
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];
            const hasNextPage = !dom.getElementProperty(paginationNode, 'class').split(' ').includes('disabled');

            if (hasNextPage) {
                const paginationAnchor = paginationNode.getElementsByTagName('a')[0];

                return encodeURI(dom.getElementProperty(paginationAnchor, 'href'));
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};