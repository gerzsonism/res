module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'card-grid',
        propertyPrice: 'price',
        propertyLink: 'subcatid',
        propertyLocation: 'street',
        paginator: 'pagination2'
    };

    const params = [
        `max-${price.max}-millio`,
        `max-${size.max}-m2`,
        'csak-fenykepes-hirdetesek'
    ].join('+');

    const service = 'Ingatlan Robot';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlanRobot', params);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: false,
        useLocation: true,
        linkInContainer: true,
        prependLinkUrl: 'https://www.ingatlanrobot.hu',
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationNode) {
                const nextButtonNode = paginationNode.getElementsByTagName('a')[1];
                const nextButtonClasses = dom.getElementProperty(nextButtonNode, 'class');
                const nextPageUrl = dom.getElementProperty(nextButtonNode, 'href');

                if (nextButtonClasses.split(' ').includes('disabled')) {
                    return null;
                }

                return nextPageUrl;
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};