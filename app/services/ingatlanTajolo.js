module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        numberOfRooms,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'property-card',
        propertyPrice: 'property-card__price',
        propertyLink: 'property-card',
        propertyLocation: 'property-card__heading',
        paginator: 'pagination'
    };

    const params = [
        `ar-${price.min}000000-${price.max}000000`,
        `alapterulet-${size.min}-${size.max}`,
        `szoba-${numberOfRooms.min}-${numberOfRooms.max}`
    ].join('+');

    const service = 'Ingatlantájoló';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlanTajolo', `+${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        baseUrl: 'https://www.ingatlantajolo.hu',
        useLocation: true,
        cardIsLink: true,
        prependLinkUrl: '',
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationNode) {
                const paginationButtons = paginationNode.childNodes;
                const nextPageNode = paginationButtons[paginationButtons.length - 1];
                const nextPageAnchor = nextPageNode.getElementsByTagName('a')[0];
                const nextPageUrl = dom.getElementProperty(nextPageAnchor, 'href');
                const anchorClass = dom.getElementProperty(nextPageAnchor, 'class');

                if (!anchorClass.split(' ').includes('pagination__link--prev')) {
                    return null;
                }

                return nextPageUrl;
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};