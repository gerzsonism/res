module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
    } = config;

    const selectors = {
        propertyCard: 'js-backToTrovit',
        propertyPrice: 'amount',
        propertyLink: 'js-item-title',
    };

    const service = 'Trovit';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlanTrovit', '');
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLinkText: true,
        prependLinkUrl: '',
        getNextPageUrl(document) {
            // Unable to verify whether site has multiple pages
            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};