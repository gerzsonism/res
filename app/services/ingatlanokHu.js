module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'estate-list-item',
        propertyPrice: 'fp_price',
        propertyLink: 'fp_price',
        paginator: 'page_navigation'
    };

    const service = 'Ingatlanok.hu';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ingatlanokHu', '');
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        baseUrl: 'https://ingatlanok.hu',
        prependLinkUrl: '',
        useCustomText(property) {
            const propertyDetailsNode = property.getElementsByClassName('prop_details')[0];

            if (propertyDetailsNode) {
                const propertyDetailsItem = propertyDetailsNode.childNodes[1];
                return propertyDetailsItem.textContent.trim();
            }
            
            return null;
        },
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationNode) {
                const nextButtonNode = paginationNode.getElementsByClassName('next')[0];
                const nextButtonClasses = dom.getElementProperty(nextButtonNode, 'class');
                const nextAnchorNode = nextButtonNode.getElementsByTagName('a')[0];
                const nextPageUrl = dom.getElementProperty(nextAnchorNode, 'href');

                if (nextButtonClasses.split(' ').includes('disabled')) {
                    return null;
                }

                return nextPageUrl;
            }


            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};