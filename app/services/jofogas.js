module.exports = async config => {
    const {
        price,
        size,
        urlBuilder,
        markdown,
        logger,
    } = config;

    const params = [
        `max_price=${price.max}000000`,
        `min_price=${price.min}000000`,
        `max_size=${size.max}`,
        `min_size=${size.min}`
    ].join('&');

    const service = 'Jófogás';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('jofogas', `?${params}`);

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        {
            title: 'Unable to crawl page, click here to see results',
            url
        }
    ];
};