module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to Kaas');

    return [
        markdown.createHeading('Kaas', urlBuilder('kaas', ''))
    ];
};