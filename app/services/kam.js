module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse
    } = config;

    const selectors = {
        propertyCard: 'type-rent',
        propertyPrice: 'price',
        propertyLink: 'property-info',
        propertyDescription: 'property-info'
    };

    const service = 'KAM Home Consulting';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('kamHome', '');
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLinkText: true,
        linkInContainer: true,
        priceInContainer: true,
        priceTagName: 'strong',
        prependLinkUrl: '',
        getNextPageUrl(document) {
            // Pagination is handled by JavaScript, added extra link at the end of links
            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
        markdown.createListItem('Unable to crawl more pages, see more results from page 2', url)
    ];
};