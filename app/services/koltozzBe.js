module.exports = async config => {
    const {
        price,
        size,
        numberOfRooms,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'listing-card',
        propertyPrice: 'item-price',
        propertyLink: 'listing-link',
        propertyDescription: 'item-type',
        paginator: 'pagination'
    };

    const params = [
        `p1=${price.min}000000`,
        `p2=${price.max}000000`,
        `a1=${size.min}`,
        `a2=${size.max}`,
        `r1=${numberOfRooms.min}`,
        `r2=${numberOfRooms.max}`,
    ].join('&');

    const service = 'Költözzbe.hu';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('koltozzBe', `?${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        baseUrl: 'https://koltozzbe.hu',
        prependLinkUrl: 'https://koltozzbe.hu',
        hasPropertyDescription: true,
        customFilter(property) {
            const propertyPrice = property.getElementsByClassName(selectors.propertyPrice)[0];

            return Number(propertyPrice.textContent.split(' ')[0]) > 1;
        },
        getPropertyDescription(property) {
            const propertyDescriptionNode = property.getElementsByClassName(selectors.propertyDescription)[0];

            return propertyDescriptionNode.textContent;
        },
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationNode) {
                const nextButtonNode = paginationNode.getElementsByClassName('btn-next')[0];
                const nextButtonAnchor = nextButtonNode.getElementsByTagName('a')[0];

                if (nextButtonAnchor) {
                    return dom.getElementProperty(nextButtonAnchor, 'href');
                }
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};