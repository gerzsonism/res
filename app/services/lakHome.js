module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'gallery__card-container',
        propertyPrice: 'estate-card__estate-price',
        propertyLink: 'estate-card',
        propertyLocation: 'estate-card__estate-address--street-or-zone',
        paginator: 'pager__next'
    };

    const service = 'LakHome';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('lakHome', '');
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        useLocation: true,
        baseUrl: 'https://lakhome.ingatlan.com',
        prependLinkUrl: 'https:',
        getNextPageUrl(document) {
            const nextPageNode = document.getElementsByClassName(selectors.paginator)[0];
            const nextPageUrl = nextPageNode && dom.getElementProperty(nextPageNode, 'href');

            return nextPageUrl;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};