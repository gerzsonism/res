module.exports = async config => {
    const {
        collectProperties,
        price,
        size,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'result-row',
        propertyPrice: 'price',
        propertyDetails: 'description',
        propertyLink: 'title-link'
    };

    const service = 'Lakások.hu';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('lakasok', `:${price.max}Mft-ig;${size.max}nm-ig;tegla-epitesu?record=0&num=100`);
    const document = await parse(url);
    const links    = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        baseUrl: 'https://lakasok.hu/elado/lakas/szeged',
        useLinkText: true,
        priceInContainer: true,
        priceTagName: 'div',
        prependLinkUrl: '',
        hasPropertyDescription: true,
        getPropertyDescription(property) {
            return property.getElementsByClassName(selectors.propertyDetails)[0].textContent;
        },
        getNextPageUrl(document) {
            const nextPageAnchorNode = document.getElementsByClassName('paginator_next')[0];

            if (nextPageAnchorNode) {
                const nextPageUrl = dom.getElementProperty(nextPageAnchorNode, 'href');
    
                if (nextPageUrl) {
                    return nextPageUrl;
                }
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links
    ];
};