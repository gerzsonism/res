module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to Limit');

    return [
        markdown.createHeading('Limit', urlBuilder('limit', ''))
    ];
};