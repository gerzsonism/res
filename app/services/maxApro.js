module.exports = async config => {
    const {
        price,
        size,
        numberOfRooms,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'srBlock',
        propertyPrice: 'srPrice',
        propertyLink: 'srData',
        paginator: 'searchResultPagination'
    };

    const params = [
        `price_${price.min}000000_${price.max}000000`,
        `size_${size.min}_${size.max}`,
        `room_${numberOfRooms.min}_${numberOfRooms.max}`,
    ].join('-');

    const service = 'MaxApró';
    let page = 1;

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('maxApro', `-${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: false,
        linkInContainer: true,
        useLinkText: true,
        baseUrl: 'https://lakhome.ingatlan.com',
        prependLinkUrl: '',
        getNextPageUrl(document) {
            const paginationNode = document.getElementById(selectors.paginator);
            const childNodes = paginationNode.childNodes;
            const lastChildElement = childNodes[childNodes.length - 2];
            const isLastPage = lastChildElement.nodeName === 'span';

            if (!isLastPage) {
                page++;

                return `${url}-p_${page}`;
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};