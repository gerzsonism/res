module.exports = async config => {
    const {
        price,
        size,
        numberOfRooms,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'hirdetes_lista_hirdetes_container',
        propertyPrice: 'hirdetes_item_ar',
        propertyLink: 'hirdetes_item_cim',
        paginator: 'PagedList-skipToNext'
    };

    const params = [
        `MinAr=${price.min}`,
        `MaxAr=${price.max}}`,
        `MinAlapterulet=${size.min}`,
        `MaxAlapterulet=${size.max}`,
        `MinSzobaszam=${numberOfRooms.min}`,
        `MaxSzobaszam=${numberOfRooms.max}`,
        'CsakKep=1'
    ].join('&');

    const service = 'MegveszLak';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('megveszLak', `?${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        linkInContainer: true,
        useLinkText: true,
        baseUrl: 'https://megveszlak.hu',
        prependLinkUrl: 'https://megveszlak.hu',
        getNextPageUrl(document) {
            const paginationNode = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationNode) {
                const nextPageAnchor = paginationNode.getElementsByTagName('a')[0];

                return dom.getElementProperty(nextPageAnchor, 'href');
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};