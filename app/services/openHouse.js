module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'item',
        propertyPrice: 'item',
        propertyLink: 'hirdetes_item_cim',
        paginator: 'pager_nums'
    };

    const service = 'OpenHouse';
    const pages = [];
    let currentPage = 0;

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('openHouse', '');
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        linkInContainer: true,
        linkIndex: 1,
        priceInContainer: true,
        priceTagName: 'strong',
        useLinkText: true,
        baseUrl: 'https://oh.hu',
        prependLinkUrl: 'https://oh.hu',
        customFilter(property) {
            const isProperty = property.getElementsByClassName('compare-prop')[0];

            return !!isProperty;
        },
        getNextPageUrl(document) {
            const paginationContainer = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationContainer) {
                if (!pages.length) {
                    const links = [...paginationContainer.childNodes].filter(node => node.nodeName === 'a');
                    
                    links.forEach(link => {
                        pages.push(dom.getElementProperty(link, 'href'));
                    });
                }

                currentPage++;

                return pages[currentPage];
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};