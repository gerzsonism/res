module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to OTP Ingatlanpont');

    return [
        markdown.createHeading('OTP Ingatlanpont', urlBuilder('otpIngatlanpont', ''))
    ];
};