module.exports = async config => {
    const {
        price,
        size,
        numberOfRooms,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse
    } = config;

    const selectors = {
        propertyCard: 'estate-list-box',
        propertyPrice: 'estate-price',
        propertyLink: 'estate-list-item'
    };

    const params = [
        `ar:${price.min}~${price.max}`,
        `netto-alapterulet:${size.min}~${size.max}`,
        `szobak:${numberOfRooms.min}~${numberOfRooms.max}`,
        'oldalszam:48',
        'rendezes:relevance',
        'felhasznalas:elado',
        'tipus:flat',
        'hely-ertek:szeged',
        'hely-id:szeged'
    ].join('/');

    const service = 'Otthon Centrum';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('otthonCentrum', `/${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLinkText: true,
        usePriceOnly: true,
        prependLinkUrl: 'https://www.oc.hu',
        getNextPageUrl(document) {
            // Can show up to 48 results per page
            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};