module.exports = async config => {
    const {
        price,
        size,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'prop-card ',
        propertyPrice: 'prop-fullprice',
        propertyLink: 'prop-link',
        propertyLocation: 'prop-address',
        paginator: 'next'
    };

    const params = [
        `${price.min}-${price.max}-mFt-ig`,
        `${size.min}-${size.max}-m2`,
    ].join('+');

    const service = 'Otthon Térkép';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('otthonTerkep', `+${params}?sort=rd&ex=1`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        baseUrl: 'https://otthonterkep.hu',
        useLocation: true,
        prependLinkUrl: '',
        getNextPageUrl(document) {
            const nextPageNode = document.getElementsByClassName(selectors.paginator)[0];

            if (nextPageNode) {
                const nextPageAnchorNode = nextPageNode.getElementsByTagName('a')[0];

                if (nextPageAnchorNode) {
                    return dom.getElementProperty(nextPageAnchorNode, 'href');
                }
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};