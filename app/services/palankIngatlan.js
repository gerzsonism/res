module.exports = async config => {
    const {
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'node-ingatlan',
        propertyPrice: 'field-name-field-vetelar',
        propertyLink: 'field-name-node-link',
        propertyDetails: 'field-name-field-ingatlan-cim',
        paginator: 'arrow'
    };

    const service = 'Palánk Ingatlan';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('palankIngatlan', '');
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        baseUrl: 'https://palankingatlan.hu',
        linkInContainer: true,
        prependLinkUrl: 'https://palankingatlan.hu',
        hasPropertyDescription: true,
        getPropertyDescription(property) {
            const propertyDescription = property.getElementsByClassName(selectors.propertyDetails)[0];

            if (propertyDescription) {
                const propertyDescriptionParagraph = propertyDescription.getElementsByTagName('p')[0].textContent;
                return propertyDescriptionParagraph;
            }

            return '';
        },
        getNextPageUrl(document) {
            const nextPageNode = document.getElementsByClassName(selectors.paginator);
            const currentPageNode = document.getElementsByClassName('current')[1];
            const currentPageNodeClasses = dom.getElementProperty(currentPageNode, 'class');
            let correctNextPageNode = nextPageNode[0];

            if (currentPageNodeClasses.split(' ').includes('last')) {
                return null;
            }

            if (nextPageNode) {
                if (nextPageNode.length !== 2) {
                    correctNextPageNode = nextPageNode[2];
                }

                const nextPageAnchorNode = correctNextPageNode.getElementsByTagName('a')[0];

                if (nextPageAnchorNode) {
                    return dom.getElementProperty(nextPageAnchorNode, 'href');
                }
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};