module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to Relax Szeged');

    return [
        markdown.createHeading('Relax Szeged', urlBuilder('otpIngatlanpont', ''))
    ];
};