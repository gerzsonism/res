module.exports = async config => {
    const {
        price,
        size,
        numberOfRooms,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'hrdts_unit',
        propertyPrice: 'narrow',
        propertyLink: 'theTitle',
        paginator: 'yiiPager'
    };

    const params = [
        `${price.min}000000-${price.max}000000`,
        `${size.min}-${size.max}`,
        `${numberOfRooms.min}-${numberOfRooms.max}`
    ].join(',');

    const service = 'Startapró';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('startApro', `/0,${params}?adverttype=all`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: true,
        baseUrl: 'https://www.startapro.hu',
        priceInContainer: true,
        priceTagName: 'div',
        useLinkText: true,
        prependLinkUrl: 'https://www.startapro.hu',
        getNextPageUrl(document) {
            const nextPageNode = document.getElementsByClassName(selectors.paginator)[0];

            if (nextPageNode) {
                const nextPageListNode = nextPageNode.getElementsByClassName('next')[0];
                const nextPageListNodeClass = dom.getElementProperty(nextPageListNode, 'class');
                const hasNextPage = !nextPageListNodeClass.split(' ').includes('hidden');

                if (hasNextPage) {
                    const nextPageAnchorNode = nextPageListNode.getElementsByTagName('a')[0];

                    return dom.getElementProperty(nextPageAnchorNode, 'href'); 
                }
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};