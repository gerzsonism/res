module.exports = async config => {
    const {
        price,
        size,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'cardContainer',
        propertyPrice: 'cardPrice',
        propertyLink: 'cardData',
        paginator: 'pagination'
    };

    const params = [
        `ar-${price.min}000000-${price.max}000000`,
        `alapterulet-${size.min}-${size.max}`
    ].join('+');

    const service = 'Startlak';
    let page = 1;

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('startlak', `+${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: false,
        linkInContainer: true,
        useLinkText: true,
        prependLinkUrl: 'https://www.startlak.hu',
        getNextPageUrl(document) {
            const paginationContainer = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationContainer) {
                const paginationItems = [...paginationContainer.childNodes].filter(node => node.nodeName === 'li');
                const lastPaginationItem = paginationItems[paginationItems.length - 1];
                const lastPaginationItemClass = dom.getElementProperty(lastPaginationItem, 'class');
                const isLastPage = lastPaginationItemClass ? 
                    lastPaginationItemClass.split(' ').includes('active') :
                    false;

                if (!isLastPage) {
                    page++;

                    return `${url}?page=${page}`;
                }
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};