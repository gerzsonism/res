module.exports = async config => {
    const {
        price,
        size,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'estate-list-box',
        propertyPrice: 'col-xs-5',
        propertyLink: 'atRealEstateList-showEstate',
        propertyLocation: 'title'
    };

    const service = 'ÚjOtthon.hu';

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ujOtthon', '');
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useLocation: true,
        prependLinkUrl: 'https://ujotthon.hu',
        getNextPageUrl(document) {
            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};