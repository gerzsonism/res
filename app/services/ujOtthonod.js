module.exports = async config => {
    const {
        price,
        size,
        collectProperties,
        urlBuilder,
        markdown,
        logger,
        parse,
        dom
    } = config;

    const selectors = {
        propertyCard: 'property-item',
        propertyPrice: 'price',
        propertyLink: 'property-item',
        paginator: 'pagination'
    };

    const params = [
        `min-price=${price.min}000000`,
        `max-price=${price.max}000000`,
        `min-area=${size.min}`,
        `max-area=${size.max}`
    ].join('&');

    const service = 'Újotthonod';
    const pages = [];
    let currentPage = 0;

    logger.progress(`Collecting estates from ${service}`);

    const url = urlBuilder('ujOtthonod', `&${params}`);
    const document = await parse(url);
    const links = await collectProperties({
        service,
        config,
        document,
        selectors,
        useBaseUrl: false,
        linkInContainer: true,
        useLinkText: true,
        prependLinkUrl: '',
        getNextPageUrl(document) {
            const paginationContainer = document.getElementsByClassName(selectors.paginator)[0];

            if (paginationContainer) {
                if (!pages.length) {
                    const links = [...paginationContainer.childNodes].filter(node => node.nodeName === 'a');
                    
                    links.forEach(link => {
                        pages.push(dom.getElementProperty(link, 'href'));
                    });
                }

                currentPage++;

                return pages[currentPage];
            }

            return null;
        }
    });

    logger.success(`Collected estates from ${service}`);

    return [
        markdown.createHeading(service, url),
        ...links,
    ];
};