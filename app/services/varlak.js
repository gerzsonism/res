module.exports = config => {
    const {
        urlBuilder,
        markdown,
        logger
    } = config;

    logger.info('Added link to VárLak Ingatlaniroda');

    return [
        markdown.createHeading('VárLak Ingatlaniroda', urlBuilder('varlakIngatlan', ''))
    ];
};