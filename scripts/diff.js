const fs = require('fs');
const logger = require('../app/lib/logger.js');

const markdown0 = fs.readFileSync('results.md', 'utf8').split('\n');
const markdown1 = fs.readFileSync('results1.md', 'utf8').split('\n');

const diff = markdown0.filter(line => !markdown1.includes(line));

if (!diff.length) {
    logger.info('The two files are identical.');
} else {
    fs.writeFileSync('diff.md', diff.join('\n'));

    logger.success(`Created diff file at: 📁 diff.md`);
    logger.info(`Number of new properties: ${diff.length}`);
}