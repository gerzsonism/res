const fs = require('fs');
const logger = require('../app/lib/logger.js');
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

if (config.blacklistResults) {   
    const regex = new RegExp(config.blacklistResults.join('|'), 'g');
    const markdown = fs.readFileSync('results.md', 'utf8').split('\n');
    const filteredMarkdown = markdown.filter(line => {
        if (!regex.test(line)) {
            return true;
        }
    
        return false;
    });
    
    fs.writeFileSync('filtered.md', filteredMarkdown.join('\n'));
    
    logger.success(`Removed ${markdown.length - filteredMarkdown.length} results and created file: 📁 filtered.md`);
    logger.info(`~${filteredMarkdown.length} results remaining`);
} else {
    logger.info('No "blacklistResults" found in config, please add terms to filter.');
}